DEVELOPMENT
-----------

- the "Development" folder contains the development keystore file, being "debug.keystore" in this case, which is a general android development keystore file.
--> Keystore name: "debug.keystore"
--> Keystore password: "android"
--> Key alias: "androiddebugkey"
--> Key password: "android"
--> CN: "CN=Android Debug,O=Android,C=US"


DISTRIBUTION
------------