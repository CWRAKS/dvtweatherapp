﻿using System;
using System.Threading.Tasks;
using DVTWeather.ViewModels;
using DVTWeather.ViewModels.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DVTWeather.UnitTests
{
    [TestClass]
    public class MainViewModelTests
    {
        [ClassInitialize]
        public static void BuildDependencies(TestContext context)
        {
            ViewModelLocator.RegisterDependencies(true);
        }

        [TestMethod]
        public async Task InitializeAsync_WeatherDataIsRecievedWithGBCountryCode_ACountryNameOfUnitedKingdomIsDisplayed()
        {
            // ARRANGE

            var mainViewModel = ViewModelLocator.Resolve<MainViewModel>();

            // ACT

            await mainViewModel.InitializeAsync(null);

            // ASSERT

            Assert.IsTrue(mainViewModel.LocationDescription.ToLower().Contains("united kingdom"));
        }
    }
}
