﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVTWeather.ViewModels;
using DVTWeather.ViewModels.Base;
using DVTWeather.Views;
using Microsoft.Azure.Mobile;
using Microsoft.Azure.Mobile.Analytics;
using Microsoft.Azure.Mobile.Crashes;
using Xamarin.Forms;

namespace DVTWeather
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            InitApp();
        }

        private void InitApp()
        {
            ViewModelLocator.RegisterDependencies(false);

            var page = ViewModelLocator.CreatePage<MainViewModel>();

            MainPage = new CustomNavigationView(page);

            (page.BindingContext as MainViewModel).InitializeAsync(null);
        }

        protected override void OnStart()
        {
            MobileCenter.Start("android=029320e7-2346-4bbd-b91f-9cd674bd6eed;" +
                               "uwp=59e1b465-5833-4301-bd2c-d9e34858657a;" +
                               "ios=196141ce-e7d3-49b0-881a-8f671d4efb2e",
                               typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
