// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace DVTWeather.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

        #region Setting Constants

        private const string OpenWeatherMapAPIBaseUrlKey = "openweatherapibaseurl_key";
        private static readonly string OpenWeatherMapAPIBaseUrlDefault = "http://api.openweathermap.org/data/2.5/weather";

        private const string OpenWeatherMapIconBaseUrlKey = "openweathericonbaseurl_key";
        private static readonly string OpenWeatherMapIconBaseUrlDefault = "http://openweathermap.org/img/w";

        private const string OpenWeatherMapAPIKeyKey = "openweatherapikey_key";
		private static readonly string OpenWeatherMapAPIKeyDefault = "d2db5a3df333185f1a6d7da76f323959";

        #endregion

        public static string OpenWeatherMapAPIBaseUrl
        {
            get
            {
                return AppSettings.GetValueOrDefault(OpenWeatherMapAPIBaseUrlKey, OpenWeatherMapAPIBaseUrlDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(OpenWeatherMapAPIBaseUrlKey, value);
            }
        }

        public static string OpenWeatherMapIconBaseUrl
        {
            get
            {
                return AppSettings.GetValueOrDefault(OpenWeatherMapIconBaseUrlKey, OpenWeatherMapIconBaseUrlDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(OpenWeatherMapIconBaseUrlKey, value);
            }
        }

        public static string OpenWeatherMapAPIKey
		{
			get
			{
				return AppSettings.GetValueOrDefault(OpenWeatherMapAPIKeyKey, OpenWeatherMapAPIKeyDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(OpenWeatherMapAPIKeyKey, value);
			}
		}
	}
}