﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using DVTWeather.Services;
using DVTWeather.Services.Dialog;
using DVTWeather.Services.Geolocator;
using DVTWeather.Services.RequestProvider;
using DVTWeather.Services.Weather;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;

namespace DVTWeather.ViewModels.Base
{
    public static class ViewModelLocator
    {
        private static IContainer _container;

        public static bool UseMockService { get; set; }

        public static void RegisterDependencies(bool useMockServices)
        {
            var builder = new ContainerBuilder();

            // View models

            builder.RegisterType<MainViewModel>();

            // Services

            builder.RegisterType<DialogService>().As<IDialogService>();
            builder.RegisterType<RequestProvider>().As<IRequestProvider>();

            if (useMockServices)
            {
                builder.RegisterInstance(new WeatherMockService()).As<IWeatherService>().SingleInstance();
                builder.RegisterInstance(new GeolocatorMockService()).As<IGeolocator>().SingleInstance();

                UseMockService = true;
            }
            else
            {
                builder.RegisterType<WeatherService>().As<IWeatherService>().SingleInstance();
                builder.RegisterInstance(CrossGeolocator.Current).As<IGeolocator>().SingleInstance();

                UseMockService = false;
            }

            if (_container != null)
            {
                _container.Dispose();
            }

            _container = builder.Build();
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public static Page CreatePage<TViewModel>() where TViewModel: ViewModelBase
        {
            var viewModelType = typeof(TViewModel);

            var viewName = viewModelType.FullName.Replace("Model", string.Empty);
            var viewModelAssemblyName = viewModelType.GetTypeInfo().Assembly.FullName;
            var viewAssemblyName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}", viewName, viewModelAssemblyName);
            Type pageType = Type.GetType(viewAssemblyName);

            if (pageType == null)
            {
                throw new Exception($"Cannot locate page type for {viewModelType}");
            }

            Page page = Activator.CreateInstance(pageType) as Page;

            var viewModel = _container.Resolve<TViewModel>();

            page.BindingContext = viewModel;

            return page;
        }
    }
}
