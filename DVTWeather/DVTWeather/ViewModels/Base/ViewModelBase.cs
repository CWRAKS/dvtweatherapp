﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVTWeather.Services.Dialog;
using Xamarin.Forms;

namespace DVTWeather.ViewModels.Base
{
    public class ViewModelBase : BindableObject
    {
        protected readonly IDialogService DialogService;

        private bool _isBusy;

        public ViewModelBase()
        {
            DialogService = ViewModelLocator.Resolve<IDialogService>();
        }

        public virtual Task InitializeAsync(object navigationData)
        {
            return Task.FromResult(false);
        }

        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }

            set
            {
                _isBusy = value;

                OnPropertyChanged();
            }
        }
    }
}
