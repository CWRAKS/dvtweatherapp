﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVTWeather.Helpers;
using DVTWeather.Services.Weather;
using DVTWeather.ViewModels.Base;
using Plugin.Geolocator.Abstractions;

namespace DVTWeather.ViewModels
{
    public class MainViewModel: ViewModelBase
    {
        private readonly IWeatherService _weatherService;
        private readonly IGeolocator _geolocatorService;

        private string _headline;
        private string _iconUrl;
        private string _minWeatherDescription;
        private string _maxWeatherDescription;
        private string _locationDescription;

        public MainViewModel(IGeolocator geolocatorService, IWeatherService weatherService)
        {
            _geolocatorService = geolocatorService;
            _weatherService = weatherService;
        }

        public override async Task InitializeAsync(object navigationData)
        {
            IsBusy = true;

            try
            {
                if(_geolocatorService.IsGeolocationEnabled && _geolocatorService.IsGeolocationAvailable)
                {
                    var currentPosition = await _geolocatorService.GetPositionAsync();

                    var weatherData = await _weatherService.GetWeatherAsync(currentPosition.Latitude, currentPosition.Longitude);

                    Headline = "TODAY, " + DateTime.Today.ToString("dd MMMM yyyy").ToUpper();
                    IconUrl = $"{Settings.OpenWeatherMapIconBaseUrl}/{weatherData.Weather[0].Icon}.png";
                    MaxWeatherDescription = $"max {weatherData.Main.TempMax}°C";
                    MinWeatherDescription = $"min {weatherData.Main.TempMin}°C";

                    var countryName = weatherData.Sys.Country.Length <= 2 ? new RegionInfo(weatherData.Sys.Country).EnglishName : weatherData.Sys.Country;

                    LocationDescription = $"{weatherData.Name}, {countryName}";
                }
                else
                {
                    await DialogService.ShowAlertAsync("GPS capabilities are not enabled or available on this device", "GPS DISABLED", "OK");
                }
            }
            catch(Exception ex)
            {
                await DialogService.ShowAlertAsync("Oops something went wrong whilst getting location / weather information.", "ERROR", "OK");
            }
            finally
            {
                IsBusy = false;
            }
        }

        public string Headline
        {
            get
            {
                return _headline;
            }

            set
            {
                _headline = value;

                OnPropertyChanged();
            }
        }

        public string IconUrl
        {
            get
            {
                return _iconUrl;
            }

            set
            {
                _iconUrl = value;

                OnPropertyChanged();
            }
        }

        public string MinWeatherDescription
        {
            get
            {
                return _minWeatherDescription;
            }

            set
            {
                _minWeatherDescription = value;

                OnPropertyChanged();
            }
        }

        public string MaxWeatherDescription
        {
            get
            {
                return _maxWeatherDescription;
            }

            set
            {
                _maxWeatherDescription = value;

                OnPropertyChanged();
            }
        }

        public string LocationDescription
        {
            get
            {
                return _locationDescription;
            }

            set
            {
                _locationDescription = value;

                OnPropertyChanged();
            }
        }
    }
}
