﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var data = WeatherData.FromJson(jsonString);
//
// For POCOs visit quicktype.io?poco
//
namespace DVTWeather.Models
{

    using Newtonsoft.Json;
    public partial class Wind
    {
        [JsonProperty("deg")]
        public long Deg { get; set; }

        [JsonProperty("speed")]
        public double Speed { get; set; }
    }
}