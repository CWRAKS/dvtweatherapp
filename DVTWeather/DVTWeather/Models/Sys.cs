﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var data = WeatherData.FromJson(jsonString);
//
// For POCOs visit quicktype.io?poco
//
namespace DVTWeather.Models
{

    using Newtonsoft.Json;
    public partial class Sys
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("sunrise")]
        public long Sunrise { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("message")]
        public double Message { get; set; }

        [JsonProperty("sunset")]
        public long Sunset { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }
    }
}