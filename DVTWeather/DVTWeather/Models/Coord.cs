﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var data = WeatherData.FromJson(jsonString);
//
// For POCOs visit quicktype.io?poco
//
namespace DVTWeather.Models
{

    using Newtonsoft.Json;
    public partial class Coord
    {
        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lon")]
        public double Lon { get; set; }
    }
}