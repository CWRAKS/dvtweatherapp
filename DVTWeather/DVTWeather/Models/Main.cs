﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var data = WeatherData.FromJson(jsonString);
//
// For POCOs visit quicktype.io?poco
//
namespace DVTWeather.Models
{

    using Newtonsoft.Json;
    public partial class Main
    {
        [JsonProperty("pressure")]
        public long Pressure { get; set; }

        [JsonProperty("temp_max")]
        public double TempMax { get; set; }

        [JsonProperty("humidity")]
        public long Humidity { get; set; }

        [JsonProperty("temp")]
        public double Temp { get; set; }

        [JsonProperty("temp_min")]
        public double TempMin { get; set; }
    }
}