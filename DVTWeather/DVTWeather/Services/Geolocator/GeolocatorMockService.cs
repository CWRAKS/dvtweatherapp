﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;

namespace DVTWeather.Services.Geolocator
{
    public class GeolocatorMockService : IGeolocator
    {
        public double DesiredAccuracy { get; set; }

        public bool IsListening
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool SupportsHeading
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsGeolocationAvailable
        {
            get
            {
                return true;
            }
        }

        public bool IsGeolocationEnabled
        {
            get
            {
                return true;
            }
        }

        public event EventHandler<PositionErrorEventArgs> PositionError;
        public event EventHandler<PositionEventArgs> PositionChanged;

        public async Task<IEnumerable<Address>> GetAddressesForPositionAsync(Position position, string mapKey = null)
        {
            throw new NotImplementedException();
        }

        public async Task<Position> GetLastKnownLocationAsync()
        {
            return new Position()
            {
                Latitude = 51.5085,
                Longitude = -0.1258
            };
        }

        public async Task<Position> GetPositionAsync(TimeSpan? timeout = null, CancellationToken? token = null, bool includeHeading = false)
        {
            return new Position()
            {
                Latitude = 51.5085,
                Longitude = -0.1258
            };
        }

        public async Task<bool> StartListeningAsync(TimeSpan minimumTime, double minimumDistance, bool includeHeading = false, ListenerSettings listenerSettings = null)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> StopListeningAsync()
        {
            throw new NotImplementedException();
        }
    }
}
