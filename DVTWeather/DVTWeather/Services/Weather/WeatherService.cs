﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVTWeather.Helpers;
using DVTWeather.Models;
using DVTWeather.Services.RequestProvider;

namespace DVTWeather.Services.Weather
{
    public class WeatherService : IWeatherService
    {
        private readonly IRequestProvider _requestProvider;

        public WeatherService(IRequestProvider requestProvider)
        {
            _requestProvider = requestProvider;
        }

        public async Task<WeatherData> GetWeatherAsync(double latitude, double longitude)
        {
            var url = new Uri(Settings.OpenWeatherMapAPIBaseUrl + $"?appid={ Settings.OpenWeatherMapAPIKey }&lat={ latitude }&lon={ longitude }&units=metric");

            return await _requestProvider.GetAsync<WeatherData>(url.ToString());
        }
    }
}
