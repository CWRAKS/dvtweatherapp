﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVTWeather.Models;
using Newtonsoft.Json;

namespace DVTWeather.Services.Weather
{
    public class WeatherMockService : IWeatherService
    {
        public async Task<WeatherData> GetWeatherAsync(double latitude, double longitude)
        {
            var json = @"{'coord':{'lon':-0.13,'lat':51.51},'weather':[{'id':803,'main':'Clouds','description':'broken clouds','icon':'04d'}],'base':'stations','main':{'temp':285.79,'pressure':1019,'humidity':82,'temp_min':284.15,'temp_max':287.15},'visibility':10000,'wind':{'speed':3.1,'deg':210,'gust':9.8},'clouds':{'all':68},'dt':1505893800,'sys':{'type':1,'id':5091,'message':0.0035,'country':'GB','sunrise':1505886246,'sunset':1505930546},'id':2643743,'name':'London','cod':200}";

            return JsonConvert.DeserializeObject<WeatherData>(json);
        }
    }
}